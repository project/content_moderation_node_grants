<?php

/**
 * @file
 * Content Moderation Node Grants access hooks.
 *
 * Emulating the default Drupal node entity_access behaviours but with
 * content_moderation integration.
 */

use Drupal\node\Entity\NodeType;
use Drupal\node\NodeInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Implements hook_node_access_records().
 *
 * @see content_moderation_node_grants_node_grants()
 * @see hook_node_access_records()
 */
function content_moderation_node_grants_node_access_records(NodeInterface $node) {
  $grants = [];
  if (\Drupal::moduleHandler()->moduleExists('domain_access')) {
    // domain_access expects/requires certain permissions to be removed:
    // https://www.drupal.org/node/2707995#domain_access_module
    return $grants;
  }

  $owner_id = $node->getOwnerId();

  /* @see \Drupal\node\NodeAccessControlHandler::acquireGrants() */
  /* @see \Drupal\node\NodeGrantDatabaseStorage::writeDefault() */
  /* @see \node_install() */
  // Add the equivalent of the default grant defined by
  // NodeAccessControlHandler::acquireGrants().
  // But more specific in order to maintain better compatibility with the
  // acb module as well as avoid conflicting with the special behaviour reserved
  // for the default "all" realm.
  if ($node->isPublished()) {
    foreach ([0, 1] as $gid) {
      $grants[] = [
        'realm' => 'content_moderation_node_grants_view',
        // Access content for anonymous and authenticated users.
        'gid' => $gid,
        'grant_view' => 1,
        'grant_update' => 0,
        'grant_delete' => 0,
      ];
    }
  }
  else {
    $grants[] = [
      'realm' => 'content_moderation_node_grants_view_any_unpublished_content',
      'gid' => 0,
      'grant_view' => 1,
      'grant_update' => 0,
      'grant_delete' => 0,
    ];
    if ($owner_id) {
      $grants[] = [
        'realm' => 'content_moderation_node_grants_view_own_unpublished_content',
        'gid' => $owner_id,
        'grant_view' => 1,
        'grant_update' => 0,
        'grant_delete' => 0,
      ];
    }
  }

  $type_id = $node->bundle();
  $grants[] = [
    'realm' => "content_moderation_node_grants_edit_any_${type_id}_content",
    'gid' => 0,
    'grant_view' => 0,
    'grant_update' => 1,
    'grant_delete' => 0,
  ];
  $grants[] = [
    'realm' => "content_moderation_node_grants_delete_any_${type_id}_content",
    'gid' => 0,
    'grant_view' => 0,
    'grant_update' => 0,
    'grant_delete' => 1,
  ];
  if ($owner_id) {
    $grants[] = [
      'realm' => "content_moderation_node_grants_edit_own_${type_id}_content",
      'gid' => $owner_id,
      'grant_view' => 0,
      'grant_update' => 1,
      'grant_delete' => 0,
    ];
    $grants[] = [
      'realm' => "content_moderation_node_grants_delete_own_${type_id}_content",
      'gid' => $owner_id,
      'grant_view' => 0,
      'grant_update' => 0,
      'grant_delete' => 1,
    ];
  }

  return $grants;
}

/**
 * Implements hook_node_grants().
 *
 * @see content_moderation_entity_access()
 * @see node_query_node_access_alter()
 * @see hook_node_grants()
 */
function content_moderation_node_grants_node_grants(AccountInterface $account, $op) {
  $grants = [];
  if (\Drupal::moduleHandler()->moduleExists('domain_access')) {
    // domain_access expects/requires certain permissions to be removed:
    // https://www.drupal.org/node/2707995#domain_access_module
    return $grants;
  }
  $account_id = $account->id();
  if ($op === 'view') {
    if ($account->hasPermission('access content')) {
      $grants['content_moderation_node_grants_view'] = $account->isAnonymous() ? [0] : [1];
    }
    if ($account->hasPermission('view any unpublished content')) {
      $grants['content_moderation_node_grants_view_any_unpublished_content'] = [0];
    }
    elseif ($account_id && $account->hasPermission('view own unpublished content')) {
      $grants['content_moderation_node_grants_view_own_unpublished_content'] = [$account_id];
    }
  }

  /* @see \Drupal\node\NodePermissions::nodeTypePermissions() */
  foreach (array_keys(NodeType::loadMultiple()) as $type_id) {
    if ($op === 'update') {
      if ($account->hasPermission("edit any ${type_id} content")) {
        $grants["content_moderation_node_grants_edit_any_${type_id}_content"] = [0];
      }
      elseif ($account_id && $account->hasPermission("edit own ${type_id} content")) {
        $grants["content_moderation_node_grants_edit_own_${type_id}_content"] = [$account_id];
      }
    }
    elseif ($op === 'delete') {
      if ($account->hasPermission("delete any ${type_id} content")) {
        $grants["content_moderation_node_grants_delete_any_${type_id}_content"] = [0];
      }
      elseif ($account_id && $account->hasPermission("delete own ${type_id} content")) {
        $grants["content_moderation_node_grants_delete_own_${type_id}_content"] = [$account_id];
      }
    }
  }

  return $grants;
}
