<?php

/**
 * @file
 * Provide views hooks for the content_moderation_node_grants module.
 */

/**
 * Implements hook_views_data_alter().
 */
function content_moderation_node_grants_views_data_alter(array &$data) {
  $data['node_field_data']['content_moderation_node_grants_updatable'] = [
    'title' => t('Updatable content'),
    'help' => t('Filter by update access.'),
    'filter' => [
      'id' => 'content_moderation_node_grants_updatable',
      'field' => 'nid',
      'label' => t('Updatable'),
      'help' => t('Filters out content which the current user cannot update. <strong>Disable SQL rewriting</strong> must be left disabled for this to work.'),
    ],
  ];
  $data['node_field_revision']['content_moderation_node_grants_updatable'] = [
    'title' => t('Updatable revisions'),
    'help' => t('Filter by update access.'),
    'filter' => [
      'id' => 'content_moderation_node_grants_updatable',
      'field' => 'vid',
      'label' => t('Updatable revisions'),
      'help' => t('Filters out revisions which the current user cannot update. <em>Disable SQL rewriting</em> must be left disabled for this to work.'),
    ],
  ];

  // Work directly on the {node_access} table (working in tandem with the
  // original 'view' node_access filter).
  /* @see \Drupal\node\NodeViewsData::getViewsData() */
  $data['node_access']['content_moderation_node_grants_updatable'] = [
    'title' => t('Update Access'),
    'help' => t('Filter by update access.'),
    'filter' => [
      'id' => 'content_moderation_node_grants_updatable',
      'help' => t('Filters out content if the current user does not have update access for it. This works outside of the <strong>Disable SQL rewriting</strong> system.'),
    ],
  ];
}
