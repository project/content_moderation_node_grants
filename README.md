CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

The Content Moderation Node Grants module provides node grant access support
for content moderated content.

Addressing Drupal core issue https://www.drupal.org/project/drupal/issues/3161658

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/content_moderation_node_grants

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/content_moderation_node_grants


REQUIREMENTS
------------

This module requires no modules outside of Drupal core's
[Content Moderation](https://www.drupal.org/docs/8/core/modules/content-moderation/overview)
module.


INSTALLATION
------------

Install the Content Moderation Node Grants module as you would normally install
a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
further information.


CONFIGURATION
-------------

There is no configuration in the Drupal administrative GUI.


MAINTAINERS
-----------

* codebymikey - https://www.drupal.org/u/codebymikey

Supporting organization:

* Zodiac Media - https://www.drupal.org/zodiac-media
