<?php

/**
 * @file
 * Provide views runtime hooks for content_moderation_node_grants.module.
 */

use Drupal\views\ViewExecutable;

/**
 * Implements hook_views_query_substitutions().
 *
 * @see content_translation_translate_access()
 */
function content_moderation_node_grants_views_query_substitutions(ViewExecutable $view) {
  $account = \Drupal::currentUser();
  return [
    '***CONTENT_MODERATION_NODE_GRANTS_CAN_TRANSLATE***' => (int) ($account->hasPermission('create content translations') || $account->hasPermission('update content translations') || $account->hasPermission('delete content translations') ||
      ($account->hasPermission('translate editable entities') /*&& $entity->access('update')*/)),
  ];
}
