<?php

namespace Drupal\content_moderation_node_grants\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\Plugin\views\query\Sql;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Views filter plugin for showing only updatable content.
 *
 * @see \Drupal\node\Plugin\views\filter\Access
 * @see https://www.drupal.org/project/views_node_access_filter
 *
 * @ingroup views_filter_handlers
 *
 * @\Drupal\views\Annotation\ViewsFilter("content_moderation_node_grants_updatable")
 */
class UpdatableFilter extends FilterPluginBase {

  /**
   * The updatable query tag.
   *
   * @var string
   */
  public const QUERY_TAG = 'content_moderation_node_grants_updatable';

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Views Handler Plugin Manager.
   *
   * @var \Drupal\views\Plugin\ViewsHandlerManager
   */
  protected $joinHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->languageManager = $container->get('language_manager');
    $instance->moduleHandler = $container->get('module_handler');
    /* $instance->joinHandler = $container->get('plugin.manager.views.join'); */

    return $instance;
  }

  /**
   * {@inheritdoc}
   *
   * Search API requires extending the custom processor to support "update".
   *
   * @see \Drupal\search_api\Plugin\search_api\processor\ContentAccess
   * We can potentially add a pre-execute event for the
   * "content_access_grants" tag.
   * @see \Drupal\search_api\Event\SearchApiEvents::QUERY_PRE_EXECUTE
   */
  public function query() {
    assert($this->query instanceof Sql);
    if ($this->table === 'node_access') {
      // Working directly from the {node_access} table filter.
      /* @see \Drupal\node\NodeGrantDatabaseStorage::alterQuery() */
      $account = $this->view->getUser();
      if (!$account->hasPermission('bypass node access')) {
        $table = 'na'; /* $this->ensureMyTable(); */
        $base_table = $this->relationship ?: $this->view->storage->get('base_table');

        // Set the subquery.
        $subquery = $this->query->getConnection()->select('node_access', $table)
          ->fields($table, ['nid']);

        $grants = $this->query->getConnection()->condition('OR');
        foreach (node_access_grants('update', $account) as $realm => $gids) {
          if (!empty($gids)) {
            $grants->condition(($this->query->getConnection()->condition('AND'))
              ->condition($table . '.gid', $gids, 'IN')
              ->condition($table . '.realm', $realm)
            );
          }
        }
        if ($grants->count()) {
          $subquery->condition($grants);
        }
        /* @see \Drupal\node\NodeGrantDatabaseStorage::checkAll() */
        $subquery->condition("[$table].[grant_update]", 1, '>=');

        // Add langcode-based filtering if this is a multilingual site.
        if ($this->languageManager->isMultilingual()) {
          $langcode = $this->getLangcode();
          // If no specific langcode to check for is given, use the grant entry
          // which is set as a fallback.
          // If a specific langcode is given, use the grant entry for it.
          if ($langcode === FALSE) {
            $subquery->condition("$table.fallback", 1, '=');
          }
          else {
            $subquery->condition("$table.langcode", $langcode, '=');
          }
        }

        $snippet = "([$base_table].[nid] = [$table].[nid])";
        /*
         * if ($this->moduleHandler->moduleExists('content_translation')) {
         * // @todo display solely translatable content as well.
         * // @see content_translation_translate_access()
         * $snippet .= ' OR (***CONTENT_MODERATION_NODE_GRANTS_CAN_TRANSLATE*** = 1)';
         * }
         */
        $subquery->where($snippet);

        $query = $this->query->getConnection()->condition('AND');
        $query->exists($subquery);

        $this->query->addWhere($this->options['group'], $query);
      }
    }
    else {
      if (!empty($this->query->options['disable_sql_rewrite'])) {
        // The view has disabled SQL rewriting.
        return;
      }
      /* @see content_moderation_node_grants_query_alter() */
      $this->ensureMyTable();
      $this->query->addTag(self::QUERY_TAG);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();
    $contexts[] = 'user.node_grants:update';

    return $contexts;
  }

  /**
   * {@inheritdoc}
   */
  public function canExpose() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function operatorForm(&$form, FormStateInterface $form_state) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['langcode'] = ['default' => ''];

    return $options;
  }

  /**
   * Returns the current language code to filter by.
   *
   * @return string|bool
   *   The current language. Returns FALSE if it's undefined.
   */
  protected function getLangcode() {
    // @todo implement me properly. But typically unnecessary since the view
    //   should typically have its own language filters set.
    return $this->options['langcode'] ?: FALSE;
  }

}
