<?php

namespace Drupal\content_moderation_node_grants\EventSubscriber;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Content Moderation Node Grants config event subscriber.
 */
class ConfigurationSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a ConfigurationSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Trigger a node access rebuild when certain configurations are updated.
   */
  public function configSave(ConfigCrudEvent $event) {
    $config = $event->getConfig();
    $config_name = $config->getName();

    $needs_rebuild = FALSE;
    // Pick up new node access permission grants.
    if (strpos($config_name, 'user.role.') === 0 && $event->isChanged('permissions')) {
      $new_permissions = array_diff($config->get('permissions'), (array) $config->getOriginal('permissions'));
      if ($new_permissions) {
        if (array_intersect($new_permissions, [
          'access content',
          'view any unpublished content',
          'view own unpublished content',
        ])) {
          $needs_rebuild = TRUE;
        }
        else {
          $node_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
          $node_types = implode('|', array_keys($node_types));
          $permission_pattern = "/^(edit|delete) (own|any) ($node_types) content$/";
          foreach ($new_permissions as $new_permission) {
            if (preg_match($permission_pattern, $new_permission)) {
              // A new node access related permission was added.
              $needs_rebuild = TRUE;
              break;
            }
          }
        }
      }
    }

    if ($needs_rebuild) {
      node_access_needs_rebuild(TRUE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::SAVE] = ['configSave'];

    return $events;
  }

}
